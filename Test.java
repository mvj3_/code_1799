//1.List转换成为数组。

List list = new ArrayList();
list.add("1");
list.add("2");
final int size =list.size();
String[] arr = (String[])list.toArray(new String[size]);

//2.数组转换成为List。

String[] arr = new String[] {"1", "2"};

List list = Arrays.asList(arr);